package com.example.johnny.myapplication;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class CrashExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Activity activity;
    public CrashExceptionHandler(Activity a) {
        activity = a;
    }


    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("crash", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(MyApplication.getInstance().getBaseContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr = (AlarmManager) MyApplication.getInstance().getBaseContext().getSystemService(Context.ALARM_SERVICE);
        if (mgr != null){
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);
        } else {
            Log.e("bcp.CRITICAL", "NO ALARM MANAGER PERSISTS ON ACTIVITY");
        }
        Log.e("bcp.CRITICAL", "EXCEPTION ON THREAD " + t.getName(), e);
        activity.finish();
        System.exit(2);
    }
}
