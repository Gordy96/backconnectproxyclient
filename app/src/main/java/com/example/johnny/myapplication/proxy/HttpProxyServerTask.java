package com.example.johnny.myapplication.proxy;

import android.util.Log;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * The http proxy server task.
 *
 * @author shuaicj 2017/09/15
 */
public class HttpProxyServerTask implements Runnable {

    private final String id;
    private final Socket socket;
    private final ExecutorService pool;
    private TaskNotifier notifier;

    public HttpProxyServerTask(String id, Socket socket, ExecutorService pool) {
        this.id = id;
        this.socket = socket;
        this.pool = pool;
    }

    public HttpProxyServerTask(String id, Socket socket, ExecutorService pool, TaskNotifier notifier) {
        this.id = id;
        this.socket = socket;
        this.pool = pool;
        this.notifier = notifier;
    }

    private static int timeout = 20000;

    @Override
    public void run() {
        this.notifier.TaskStarted();
        try (final InputStream clientInput = new BufferedInputStream(socket.getInputStream());
             final OutputStream clientOutput = socket.getOutputStream()) {

            HttpProxyClientHeader header = HttpProxyClientHeader.parseFrom(clientInput);
            Log.i("bcp.s.ps.hpst",id + " " + header);

            try (final Socket remoteSocket = new Socket(header.getHost(), header.getPort())) {
                socket.setKeepAlive(false);
                socket.setTcpNoDelay(true);
                socket.setReuseAddress(true);
                socket.setSoTimeout(timeout);
                remoteSocket.setKeepAlive(false);
                remoteSocket.setTcpNoDelay(true);
                remoteSocket.setReuseAddress(true);
                remoteSocket.setSoTimeout(timeout);
                final OutputStream remoteOutput = remoteSocket.getOutputStream();
                final InputStream remoteInput = remoteSocket.getInputStream();

                if (header.isHttps()) { // if https, respond 200 to create tunnel, and do not forward header
                    if (header.getProto().equalsIgnoreCase("HTTP/1.1"))
                        clientOutput.write("HTTP/1.1 200 Connection Established\r\n\r\n".getBytes());
                    else
                        clientOutput.write("HTTP/1.0 200 Connection Established\r\n\r\n".getBytes());
                    clientOutput.flush();
                } else { // if http, forward header
                    remoteOutput.write(header.getBytes());
                }

                Future<?> future = pool.submit(new Callable<Object>() {
                    @Override
                    public Object call() {
                        pipeIn(clientInput, remoteOutput);
                        return null;
                    }
                });
                pipeOut(remoteInput, clientOutput);
                future.get();
                try {
                    closeSocket(remoteSocket);
                } catch (IOException e) {}
            } finally {
                try {
                    closeSocket(socket);
                } catch (IOException e) {}
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            Log.e("bcp.s.ps.hpst",e.getMessage(), e);
        } finally {
            this.notifier.TaskDone();
        }
    }

    private void pipeIn(InputStream in, OutputStream out) {
        byte[] buf = new byte[4096];
        int len;
        try {
            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.flush();
        } catch (IOException ignored) {}
    }

    private void pipeOut(InputStream in, OutputStream out) {
        byte[] buf = new byte[4096];
        int len;
        try {
            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.flush();
        } catch (IOException ignored) {}
    }

    public static void closeSocket(Socket s) throws IOException {
        try {
            s.getOutputStream().flush();
        } finally {
            try {
                s.getOutputStream().close();
            } finally {
                try {
                    s.getInputStream().close();
                } finally {
                    s.close();
                }
            }
        }
    }
}
