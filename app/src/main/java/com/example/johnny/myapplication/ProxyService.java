package com.example.johnny.myapplication;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.johnny.myapplication.proxy.HttpProxyServer;

import java.net.InetSocketAddress;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

public class ProxyService extends Service {
    public static final String NETWORK_RESTART_ACTION = "com.example.johnny.myapplication.network.restart";

    private InetSocketAddress ct;
    private String tag;
    private ClientHandler thread;
    private HttpProxyServer proxy = null;
    private WifiManager manager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        super.onCreate();

    }

    private boolean should_retry = true;
    private static final String cid     = "proxy_channel_1337";
    private static final int    fsid    = 1337;

    private Notification buildForegroundNotification(int current_tasks) {
        NotificationCompat.Builder b=new NotificationCompat.Builder(this, cid);

        b.setOngoing(true)
                .setContentTitle("proxy server")
                .setContentText(String.format("current connections: %d", current_tasks))
                .setSmallIcon(android.R.drawable.stat_notify_sync)
                .setTicker("proxy started");

        return(b.build());
    }

    private void StartManagerThread(){
        try{
            if(thread != null)
                thread.kill();
            thread = new ClientHandler(ct, tag, proxy);
            thread.addHandler(new ControlSocketDisconnectHandler() {
                @Override
                public void Handle() {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(should_retry){
                                Log.i("bcp.s.ps.rc", "trying reconnect");
                                StartManagerThread();
                            }
                        }
                    }, 5000);
                }
            });
            thread.addHandler(new ControlSocketCommandHandler() {
                @Override
                public boolean Handle(String command) {
                    switch (command){
                        case "restart_network":
                            SendRestartNetworkEvent();
                            return true;
                    }
                    return false;
                }
            });

            thread.start();

            new Thread() {
                @Override
                public void run() {
                    while(true){
                        try {
                            startForeground(fsid, buildForegroundNotification(proxy.activeTasks()));
                            Thread.sleep(2000);
                        } catch (InterruptedException e){
                            Log.e("bcp.s.ps.rc", e.getMessage(), e);
                        }
                    }
                }
            }.start();

            Log.i("bcp.s.ps.rc", "started ClientHandler");
        } catch (Exception e){
            Log.e("bcp.s.ps.s", e.getMessage(), e);
        }
    }

    private void StartProxy(){
        if(proxy != null){
            try{
                proxy.stop();
            } catch (IllegalStateException e){
                Log.e("bcp.s.ps.sp", e.getMessage(), e);
            }
        }
        proxy = new com.example.johnny.myapplication.proxy.HttpProxyServer(9666,  Executors.newCachedThreadPool());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        ct = (InetSocketAddress)intent.getExtras().get("connect_to");
        tag = (String)intent.getExtras().get("tag");
        should_retry = true;

        StartProxy();
        StartManagerThread();

        startForeground(fsid, buildForegroundNotification(0));


        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        should_retry = false;
        try{
            if(thread != null)
                thread.kill();
            if(proxy != null)
                proxy.stop();
        } catch (Exception e){
            Log.e("bcp.s.ps.d", e.getMessage(), e);
        }
        super.onDestroy();
        stopForeground(true);
    }

    public void SendRestartNetworkEvent(){
        //Intent i = new Intent(NETWORK_RESTART_ACTION);
        //sendBroadcast(i);
        manager.setWifiEnabled(true);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                manager.setWifiEnabled(false);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        StartProxy();
                        StartManagerThread();
                    }
                }, 8000);
            }
        }, 8000);
    }
}
