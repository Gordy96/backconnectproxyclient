package com.example.johnny.myapplication;

/**
 * Created by Johnny on 2018-11-08.
 */

public abstract class ControlSocketDisconnectHandler {
    public abstract void Handle();
}
