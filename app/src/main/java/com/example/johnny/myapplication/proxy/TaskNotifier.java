package com.example.johnny.myapplication.proxy;

public interface TaskNotifier {
    public void TaskStarted();
    public void TaskDone();
}
