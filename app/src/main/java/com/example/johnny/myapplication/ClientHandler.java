package com.example.johnny.myapplication;

import android.util.Log;

import com.example.johnny.myapplication.proxy.HttpProxyServer;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class ClientHandler extends Thread {
    private static final int MAX_SKIP_BUFFER_SIZE = 2048;
    private static final int DEFAULT_BUFFER_SIZE = 8192;

    final private InetAddress ct;
    private InetAddress st = null;
    final private int[] ports;

    private Socket connection;
    private String tag;
    private HttpProxyServer proxy;

    private boolean running = false;

    public synchronized boolean isRunning() {
        return running;
    }

    public synchronized void setRunning(boolean r) {
        running = r;
    }

    public ClientHandler(InetSocketAddress address, InetAddress send_to, int send_port, String t) {
        super();
        ct = address.getAddress();
        st = send_to;
        ports = new int[2];
        ports[0] = address.getPort();
        ports[1] = send_port;
        tag = t;
    }

    public ClientHandler(InetSocketAddress address, String tag, HttpProxyServer proxyServer) {
        super();
        this.ct = address.getAddress();
        ports = new int[2];
        ports[0] = address.getPort();
        this.tag = tag;
        proxy = proxyServer;
    }

    @Override
    public void run() {
        try{
            setRunning(true);
            Log.i("bcp.s.clh.r", "trying connect to " + ct.getHostAddress());
            connection = new Socket(ct, ports[0]);
            Log.i("bcp.s.clh.r", "connected to " + ct.getHostAddress());
            connection.getOutputStream().write(tag.getBytes());
            connection.setSoTimeout(15000);
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int read;
            int noservercommands = 0;
            while(true){
                try{
                    read = connection.getInputStream().read(buffer, 0, DEFAULT_BUFFER_SIZE);
                    if (read < 0)
                        throw new SocketException("control socket is closed or closing (read returned -1)");
                } catch (SocketTimeoutException e) {
                    if (noservercommands > 10)
                        throw new Exception("reconnection due to long time idling (might lost connection)");
                    Log.e("bcp.s.clh.r", "could not read form control socket, trying again");
                    noservercommands++;
                    continue;
                }
                noservercommands = 0;
                StringBuilder str = new StringBuilder();
                for(int i = 0; i < read; i++){
                    str.append((char)buffer[i]);
                }
                String[] parts = str.toString().split("\\|");
                Log.i("bcp.s.clh.r", "received command: \"" + str + "\"");
                switch(parts[0]){
                    case "create_socket":
                        Socket srvSock = null;
                        long start = System.currentTimeMillis();
                        while(srvSock == null) {
                            try {
                                srvSock = new Socket(ct, Integer.valueOf(parts[1]));
                            } catch (Throwable e) {
                                if (System.currentTimeMillis() - start > 60000)
                                    break;
                                Thread.sleep(10);
                            }
                        }
                        if (srvSock == null){
                            throw new Exception("could not open new socket to server");
                        }
                        proxy.ServerSocket(srvSock);
                        break;
                    default:
                        boolean handled = commandHandler.Handle(parts[0]);
                        if (!handled)
                            throw new SocketException("received unknown command: \"" + str + "\"");
                }
            }
        } catch(Throwable e){
            Log.e("bcp.s.clh.r", e.getMessage(), e);
            setRunning(false);
            handler.Handle();
        } finally {
            try{
                if (connection != null)
                    connection.close();
            } catch (Exception e){
                Log.e("bcp.s.clh.rf", e.getMessage(), e);
            }
        }
    }
    private ControlSocketDisconnectHandler handler;
    private ControlSocketCommandHandler commandHandler;

    public void addHandler(ControlSocketDisconnectHandler h){
        handler = h;
    }
    public void addHandler(ControlSocketCommandHandler h){
        commandHandler = h;
    }

    public void kill(){
        try{
            if (connection != null)
                connection.close();
        } catch (Exception ex){
            Log.e("bcp.s.clh.k", ex.getMessage(), ex);
        }
    }
}
