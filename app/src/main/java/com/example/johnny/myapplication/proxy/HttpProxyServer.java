package com.example.johnny.myapplication.proxy;

import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/**
 * A simple http proxy server.
 *
 * @author shuaicj 2017/09/15
 */

public class HttpProxyServer implements TaskNotifier {

    private final int port;
    private final ExecutorService pool;
    private ServerSocket serverSocket;
    private long taskCount;

    public HttpProxyServer(int port, ExecutorService pool) {
        this.port = port;
        this.pool = pool;
    }

    @Override
    public void TaskDone() {
        this.decActiveTasks();
    }

    @Override
    public void TaskStarted() {
        this.incActiveTasks();
    }

    public void ServerSocket(Socket socket) {
        pool.submit(new HttpProxyServerTask("task-" + taskCount++, socket, pool, this));
    }

    private boolean should_stop = false;

    public void stop() {
        should_stop = true;
        try{
            if (serverSocket != null)
                serverSocket.close();
        } catch (IOException e) {
            Log.e("bco.pss.start", e.getMessage(), e);
        }
    }

    private int activeTasks = 0;

    public void incActiveTasks() {
        synchronized (this) {
            activeTasks++;
        }
    }

    public void decActiveTasks() {
        synchronized (this) {
            activeTasks--;
        }
    }

    public int activeTasks() {
        synchronized (this) {
            return activeTasks;
        }
    }



    public void start() throws IOException {
        serverSocket = new ServerSocket(port);
        pool.submit(new Callable<Object>() {
            @Override
            public Object call() {
                while (!should_stop) {
                    try {
                        Socket socket = serverSocket.accept();
                        pool.submit(new HttpProxyServerTask("task-" + taskCount++, socket, pool));
                    } catch (IOException e) {
                        Log.e("bco.pss.start", e.getMessage(), e);
                    }
                }
                return null;
            }
        });
    }
}
