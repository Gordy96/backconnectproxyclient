package com.example.johnny.myapplication.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The http header of client.
 *
 * @author shuaicj 2017/09/15
 */

public class HttpProxyClientHeader {

    private String method;
    private String host;
    private int port;
    private boolean https;
    private String path;
    private String proto;

    private HashMap<String, String> headers = new HashMap<>();

    private final InputStream in;
    private final ByteArrayOutputStream consumedBytes;

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public String getPath() {
        return path;
    }

    public String getProto() {
        return proto;
    }

    public void setProto(String proto) {
        this.proto = proto;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public boolean isHttps() {
        return https;
    }

    public String getMethod() {
        return method;
    }

    public byte[] getBytes() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s %s %s\r\n", method, path, proto));
        for (String header : headers.keySet())
            sb.append(String.format("%s: %s\r\n", header, headers.get(header)));
        String ret = sb.append("\r\n").toString();
        return ret.getBytes();
    }

    private HttpProxyClientHeader(InputStream in) {
        this.in = in;
        this.consumedBytes = new ByteArrayOutputStream();
    }

    public static HttpProxyClientHeader parseFrom(InputStream in) throws IOException {
        HttpProxyClientHeader header = new HttpProxyClientHeader(in);
        header.init();
        return header;
    }

    private void addHeader(String line) {
        String[] arr = line.split(":", 2);
        headers.put(arr[0].trim(), arr[1].trim());
    }

    private void init() throws IOException {
        String[] firstLine = readLine().split(" ");
        method = firstLine[0];// the first word is http method name
        path = firstLine[1];
        proto = firstLine[2];

        https = method.equalsIgnoreCase("CONNECT"); // method CONNECT means https
        for (String line = readLine(); line != null && !line.isEmpty(); line = readLine()) {
            if (line.startsWith("Host: ")) {
                String[] arr = line.split(":");
                host = arr[1].trim();
                try {
                    if (arr.length == 3) {
                        port = Integer.parseInt(arr[2]);
                    } else if (https) {
                        port = 443; // https
                    } else {
                        port = 80; // http
                    }
                } catch (NumberFormatException e) {
                    throw new IOException(e);
                }
            }
            addHeader(line);
        }
        if (host == null || port == 0) {
            throw new IOException("cannot find header \'Host\'");
        }
    }

    private String readLine() throws IOException {
        StringBuilder builder = new StringBuilder();
        for (int b = in.read(); b != -1; b = in.read()) {
            consumedBytes.write(b);
            builder.append((char) b);
            int len = builder.length();
            if (len >= 2 && builder.substring(len - 2).equals("\r\n")) {
                builder.delete(len - 2, len);
                return builder.toString();
            }
        }
        return builder.length() == 0 ? null : builder.toString();
    }
}
