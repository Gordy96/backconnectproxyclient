package com.example.johnny.myapplication;

/**
 * Created by Johnny on 2018-11-23.
 */

public abstract class ControlSocketCommandHandler {
    public abstract boolean Handle(String command);
}
