package com.example.johnny.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
public class MainActivity extends AppCompatActivity {

    private int port = 9090;

    private Button listen;
    private Button stop;

    private WifiManager manager;
    private SharedPreferences pref;

    private EditText tag_input;
    private EditText lip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new CrashExceptionHandler(this));

        listen = findViewById(R.id.listen);
        stop = findViewById(R.id.stop);

        setEnabledStart(true);
        setEnabledStop(false);

        pref = PreferenceManager.getDefaultSharedPreferences(this);

        tag_input = findViewById(R.id.tag_input);
        lip = findViewById(R.id.lip);

        tag_input.getText().clear();
        tag_input.getText().append(pref.getString("tag", ""));

        lip.getText().clear();
        lip.getText().append(pref.getString("proxy_server", ""));

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
            }
        });

        manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        Button toggle = findViewById(R.id.toggle);
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(manager.isWifiEnabled()){
                    manager.setWifiEnabled(false);
                } else {
                    manager.setWifiEnabled(true);
                }
            }
        });

        if(getIntent().getBooleanExtra("crash", false)) {
            start();
        }
    }

    void start() {
        String tag = pref.getString("tag", "");
        String newTag = tag_input.getText().toString();

        if (!newTag.equals(tag)){
            tag = newTag;
            SharedPreferences.Editor edit = pref.edit();
            edit.putString("tag", tag);
            edit.commit();
        }

        String ipString = pref.getString("proxy_server", "");
        String newIpString = lip.getText().toString();

        if (!newIpString.equals(ipString)){
            ipString = newIpString;
            SharedPreferences.Editor edit = pref.edit();
            edit.putString("proxy_server", ipString);
            edit.commit();
        }

        InetSocketAddress ip = parseIp(ipString);

        if(ip == null) {
            Toast.makeText(MainActivity.this, "Invalid proxy server ip", Toast.LENGTH_SHORT).show();
        } else {
            if(tag.length() > 0){
                startProxyService(ip, tag);
                setEnabledStart(false);
                setEnabledStop(true);
            } else {
                Toast.makeText(MainActivity.this, "Tag is too short", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void stop() {
        stopProxyService();
        setEnabledStart(true);
        setEnabledStop(false);
    }

    void setEnabledStart(boolean en){
        listen.setEnabled(en);
    }
    void setEnabledStop(boolean en){
        stop.setEnabled(en);
    }

    void startProxyService (InetSocketAddress ip, String tag) {
        stopProxyService();
        Intent i = new Intent(this, ProxyService.class);
        i.putExtra("tag", tag);
        i.putExtra("connect_to", ip);
        startService(i);

    }

    void stopProxyService () {
        Intent i = new Intent(this, ProxyService.class);
        stopService(i);
    }

    private InetSocketAddress parseIp(String ip) {
        String[] parts = ip.split(":");
        byte[] addr = new byte[4];
        int port = 9090;
        if(parts.length > 1 && parts[1].length() > 0)
            port = Integer.valueOf(parts[1]);
        if (!ip.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(?::[0-9]{2,5})?\\b"))
            return null;
        parts = parts[0].split("\\.");
        for(int i = 0; i < parts.length; i++){
            addr[i] = (byte)(int)Integer.valueOf(parts[i]);
        }
        InetSocketAddress address;
        try{
            address = new InetSocketAddress(InetAddress.getByAddress(addr), port);
        } catch (UnknownHostException e) {
            return null;
        }
        return address;
    }

    @Override
    protected void onDestroy(){
        stopProxyService ();
        super.onDestroy();
    }
}
